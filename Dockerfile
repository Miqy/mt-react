FROM nginx:1.15.2-alpine


COPY build /var/www
COPY nginx/proxy.conf /etc/nginx/proxy.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
# CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/nginx.conf && nginx -g 'daemon off;'
CMD nginx -g 'daemon off;'
