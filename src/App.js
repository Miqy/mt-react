import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import { Home } from './views/home/home'
import UserInfo from './views/user-info/user-info'
import Login from './views/login/login'
import { requireAuth, RequireAuthRoute, LoginRedirectRoute } from './routes'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee, faPlusCircle, faBriefcase, faEdit, faBook, faGripLinesVertical, faStream, faCalendar, faTasks, faUser, faEnvelope, faHistory, faBars } from '@fortawesome/free-solid-svg-icons'
import ProjectDetail from './views/project-detail/project-detail';
import Register from './views/register/register';
import { ServerError } from './views/server-error/server-error';

library.add(fab, faCheckSquare, faCoffee, faPlusCircle, faBriefcase, faEdit, faBook, faGripLinesVertical, faStream, faCalendar, faTasks, faUser, faEnvelope, faHistory, faBars)


function App() {
  return (
    <Router>
      <div className="container">
        <RequireAuthRoute path="/home" component={<Home/>}/>
        <RequireAuthRoute path="/user_info" component={<UserInfo/>}/>
        <RequireAuthRoute path="/" component={<Home/>}/>
        <LoginRedirectRoute path="/login" component={<Login/>} />
        <LoginRedirectRoute path="/register" component={<Register/>} />
        <Route path="/project/:id" render={(props, history) => {return requireAuth(history, <ProjectDetail id={props.match.params.id}/>)}}/>
        <Route path="/error" component={ServerError}></Route>
      </div>
    </Router>
  );
}

export default App;
