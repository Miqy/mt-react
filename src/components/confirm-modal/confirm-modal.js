import React, { Component } from 'react'
import {Modal, Button} from 'react-bootstrap';



export class ConfirmModal extends Component {

    render() {
        return (
            <Modal show={this.props.show} onHide={() => {this.props.closeHandler()}} size="lg" animation="true" centered
            >
                <Modal.Header closeButton>
                  <Modal.Title>
                    <h4>{this.props.title}</h4>
                  </Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                        <p>{this.props.description}</p>
                        <Button variant="danger" onClick={this.props.yesHandler}>Tak</Button>
                        <Button variant="success" onClick={this.props.closeHandler}>Nie</Button>
                    </Modal.Body>
              </Modal>
        )
    }
}

export default ConfirmModal;
