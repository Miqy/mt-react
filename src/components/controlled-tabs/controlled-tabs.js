import React from 'react';
import {Tabs, Tab} from 'react-bootstrap'
import {useState} from 'react'


function ControlledTabs(props) {
    const [key, setKey] = useState(props.names[0]);
    
    let tabs = []
    for (let i = 0; i< props.names.length; i++){
        let name = props.names[i];
        let comp = props.components[i];
        tabs.push(
            <Tab eventKey={name} title={name} key={i}>
                {comp}
            </Tab>
        )
    }

    return (
      <Tabs defaultActiveKey={props.names[0]} id={props.menuTabs} activeKey={key} onSelect={k => setKey(k)}>
        {tabs}
      </Tabs>
    );
  }

export default ControlledTabs;