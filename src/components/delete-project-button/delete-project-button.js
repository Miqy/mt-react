import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';
import ConfirmModal from '../confirm-modal/confirm-modal';



export class DeleteProjectButton extends Component {

    constructor(props){
        super(props)
        this.state = {
             isModalOpen: false
        }
    }


    deleteProject = () => {
        fetch(`/api/tasks/projects/${this.props.project.pk}/`, {
            method: 'DELETE',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'X-CSRFToken': Cookies.get('csrftoken')                 
            },
          }).then(res => {
            if (res.status === 204) {
                this.props.history.push({
                    pathname: '/',
                    state: {actionModalMessage: "Pomyślnie usunąłeś projekt."}
                  })
            } else{
                this.props.history.push('/error')
            }
            return res
          }).catch(error => {
            this.props.history.push('/error')
          })
    }

    render() {
        return (
          <div>
            <Button variant="danger" onClick={() => {this.setState({isModalOpen: true})}}>Usuń Projekt</Button>
            <ConfirmModal show={this.state.isModalOpen} closeHandler={() => {this.setState({isModalOpen: false})}}
             title="Usuwanie projektu" description="Czy chcesz usunąć projekt?" yesHandler={this.deleteProject}/>
          </div>
        )
    }
}


export default withRouter(DeleteProjectButton)