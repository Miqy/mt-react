import React, { Component } from 'react';
import {Button, Alert} from 'react-bootstrap';
import AsyncSelect from 'react-select/async';
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';



export class InviteMemberForm extends Component {

    constructor(props){
        super(props)
        this.state = {
             inputValue: '',
             options: [],
             user: null,
             errors: [],
             invited: null,
        }
        this.fetchUsers('')
    }

    fetchUsers = (username) => {
        let exclude_users_id = []
        exclude_users_id.push(this.props.project.owner.pk)
        for (let i=0; i<this.props.project.members.length; i++){
            exclude_users_id.push(this.props.project.members[i].pk)
        }
        fetch(`/api/users/auth/users/?username=${username}&username_lookup=icontains&id_exclude=${exclude_users_id.join('%2C')}`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            let options = []
                            for (let i = 0; i < data.length; i++){
                                options.push({value: data[i].pk, label: data[i].username})
                            }

                            this.setState(
                                {
                                    options: options,
                                }
                            )
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/login")
                }
            }
        )
    }

    filterUsers = (inputValue) => {
        return this.state.options.filter(i =>
            i.label.toLowerCase().includes(inputValue.toLowerCase())
        );
    };
    
    loadOptions = (inputValue, callback) => {
        setTimeout(() => {
            callback(this.filterUsers(inputValue));
        }, 1000);
    };

    handleInputChange = (newValue) => {
        this.setState({ inputValue: newValue });
        return newValue;
    };

    inviteUser = (user) => {
        fetch(`/api/tasks/projects/${this.props.project.pk}/members_add/`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'X-CSRFToken': Cookies.get('csrftoken')                 
            },
           body: JSON.stringify({
              members: [this.state.user.value],
            })
          }).then(res => {
            if (res.status === 201) {
                this.setState({
                    invited: this.state.user.label,
                    errors: []
                })
                this.fetchUsers('')
            } else if (res.status === 400){
              res.json().then(data => {
                  this.setState({errors: [data['members']], invited: null})
              });
            } else if (res.status === 403){
              res.json().then(data => {
                  console.log(data);
                }
              )
            }
            return res
          }).catch(error => {
            console.log(error);
          })
    }

    renderErrors = () => {
        if (this.state.errors.length){
            return <Alert variant="danger">{this.state.errors[0]}</Alert>
        }
        return <></>
    }

    renderSuccess = () => {
        if (this.state.invited === null){
            return <></>
        }
        return (
            <Alert variant="success">Zaproszenie zostało wysłane do użytkownika {this.state.invited}.</Alert>
        )
    }

    render() {
        return (
            <div className="project-members-add-modal-container">
                {this.renderErrors()}
                {this.renderSuccess()}
                <AsyncSelect
                cacheOptions
                loadOptions={this.loadOptions}
                defaultOptions
                onInputChange={this.handleInputChange}
                onChange={(value) => this.setState({user: value})}
                loadingMessage={() => {return "Wczytywanie..."}}
                noOptionsMessage={() => {return "Brak dostępnych uzytkowników."}}
                placeholder={<div>Podaj nazwę użytkownika</div>}
                />
                <Button variant="success"onClick={() => {this.inviteUser()}} disabled={this.state.user === null}>Zaproś</Button>
            </div>
        )
    }
}


export default withRouter(InviteMemberForm)