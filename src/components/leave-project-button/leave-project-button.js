import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';
import ConfirmModal from '../confirm-modal/confirm-modal';



export class LeaveProjectButton extends Component {

    constructor(props){
        super(props)
        this.state = {
            isModalOpen: false
       }
    }


    leaveProject = () => {
        fetch(`/api/tasks/projects_members?member=${this.props.user.pk}&project=${this.props.project.pk}`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        membershipData => {
                            if(membershipData.length === 0){
                                this.props.history.push({
                                    pathname: '/',
                                    state: {actionModalMessage: "Pomyślnie opuściłeś projekt."}
                                  })
                            }
                            fetch(`/api/tasks/projects_members/${membershipData[0].pk}/`, {
                                method: 'DELETE',
                                headers: {
                                  'Content-Type': 'application/json',
                                  'Accept': 'application/json',
                                  'X-CSRFToken': Cookies.get('csrftoken')                 
                                },
                              }).then(res => {
                                if (res.status === 204) {
                                    this.props.history.push({
                                        pathname: '/',
                                        state: {actionModalMessage: "Pomyślnie opuściłeś projekt."}
                                      })
                                } else{
                                    console.log(res)
                                }
                                return res
                              }).catch(error => {
                                console.log(error);
                              })
                        }
                    )
                }else{
                    console.log(res);
                    this.props.history.push("/")
                }
            }
        )
    }

    render() {
        return (
            <div>
                <Button variant="danger" onClick={() => {this.setState({isModalOpen: true})}}>Opuść Projekt</Button>
                <ConfirmModal show={this.state.isModalOpen} closeHandler={() => {this.setState({isModalOpen: false})}}
                title="Opuszczanie projektu" description="Czy chcesz opuścić projekt?" yesHandler={this.leaveProject}/>
            </div>
        )
    }
}


export default withRouter(LeaveProjectButton)