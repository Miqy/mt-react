import React from 'react';
import {Navbar, NavDropdown, Spinner } from 'react-bootstrap'
import { withRouter } from 'react-router-dom';
import "./navbar.css";


export class NavbarComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            user: null
        }
    }
    componentDidMount() {
        this.getUserInfo()
    }

    getUserInfo() {
        fetch("/api/users/auth/user_info/").then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    user: data,
                                    loading: false
                                }
                            )
                        }
                    )
                }else{
                    this.props.history.push("/login")
                }
            }
        )
    }

    logout = () => {
        fetch("/api/users/auth/logout/").then(res => {
            if (res.status === 204) {
              this.props.history.push('/login')
            }
            return res
          }).catch(error => {
            console.log(error);
          })
    }

    render() {
        if (this.state.loading === true){
            return (
                <Navbar bg="light" expand="lg" className="navbar-container">
                    <Navbar.Brand href="/home">Meety</Navbar.Brand>
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Spinner animation="border" role="status" className="ml-auto">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                    </Navbar.Collapse>
                </Navbar>
            )
        }
        return (
            <Navbar expand="lg" className="navbar-container">
                <Navbar.Brand href="/home">
                    <div className="logo-nav"></div>
                    </Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    <NavDropdown title={"Jesteś zalogowany jako " + this.state.user.username} id="basic-nav-dropdown" className="ml-auto">
                        <NavDropdown.Item href="/user_info">Profil</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#logout" onClick={this.logout}>Wyloguj się</NavDropdown.Item>
                    </NavDropdown>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default withRouter(NavbarComponent);