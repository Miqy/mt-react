import React, { Component } from 'react'
import {Card, Form, Button} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';


class ProjectCardForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isFormOpen: false,
            name: "",
            isRequested: false,
        }
    }

    createNewProject = () => {
        if(this.state.isFormOpen === false){
            this.setState({
                isFormOpen: true
            })
        }
    }

    validateForm() {
        return this.state.name.length > 0 && !this.state.isRequested;
      }

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
      }

    handleSubmit = event => {
        this.setState({
          isRequested: true
        })
        if (this.state.isRequested === true){
          return
        }
        event.preventDefault();
        fetch("/api/tasks/projects/", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'X-CSRFToken': Cookies.get('csrftoken')
            },
           body: JSON.stringify({
              name: this.state.name,
            })
          }).then(res => {
            this.setState({
              isRequested: false
            })
            console.log(res);
            if (res.status === 201) {
              window.location.reload()
            } else if (res.status === 400){
              res.json().then(data => {
                console.log(data);
              });
            } else if (res.status === 403){
              res.json().then(data => {
                  console.log(data);
                }
              )
            }
            return res
          }).catch(error => {
            this.setState({
              isRequested: false
            })
            console.log(error);
          })
    }

    render() {
        if(this.state.isFormOpen === true){
            return (
                <li className='project-card-container' key={-1} onClick={this.createNewProject}>
                    <Card style={{ width: '18rem' }} >
                        <Card.Body>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="name">
                                    <Form.Control type="text" placeholder="Podaj nazwę projektu"
                                        value={this.state.name}
                                        onChange={this.handleChange}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit" disabled={!this.validateForm()}>
                                    Utwórz
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </li>
            )
        }else{
            return (
                <li className='project-card-container' key={-1} onClick={this.createNewProject}>
                    <Card style={{ width: '18rem' }} >
                        <Card.Body>
                            <Card.Title>Dodaj nowy projekt</Card.Title>
                            <FontAwesomeIcon icon="plus-circle" size='2x'/>
                        </Card.Body>
                    </Card>
                </li>
            )
        }
    }
}

export default withRouter(ProjectCardForm);