import React from 'react';
import {Card} from 'react-bootstrap'
import "./project-card.css";
import { withRouter } from 'react-router-dom';


class ProjectCard extends React.Component {

    goToProjectDetailView = () => {
        this.props.history.push(`/project/${this.props.id}`)
    }

    render() {
        return (
            <li className='project-card-container' onClick={this.goToProjectDetailView}>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title>{this.props.name}</Card.Title>
                        <Card.Text>
                            {this.props.description}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </li>
        )
    }
}

export default withRouter(ProjectCard);