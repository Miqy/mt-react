import React, { Component } from 'react';
import {Modal, Table, Spinner, Button, Alert} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import "./project-invitations-modal.css"
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';


export class ProjectInvitationsModal extends Component {

    constructor(props){
        super(props)
        this.state = {
            invitations: null,
            loading: true,
            showSuccessInvitationAccept: false,
            ShowSuccessInvitationDecline: false,
        }
        this.fetchInvitations()
    }

    fetchInvitations = () => {
        fetch(`/api/tasks/projects_members/?member=${this.props.user.pk}&invite=true`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    invitations: data,
                                    loading: false
                                }
                            )
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/")
                }else{
                    console.log(res);
                }
            }
        )
    }

    renderInvitations = () => {
        if (this.state.loading === true){
            return (
                <Spinner animation="border" role="status" className="">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )
        }
        if (this.state.invitations.length === 0){
            return (
                <p>Nie masz w tej chwili żadnych zaproszeń.</p>
            )
        }
        function nullValueFunction(value) {
            return value === null || value === "" ? "-" : value
        }
        let invitations = []
        for (let i=0; i < this.state.invitations.length; i++){
            let invitation = this.state.invitations[i]
            invitations.push(
                <tr key={i}>
                    <td>{nullValueFunction(invitation.project.name)}</td>
                    <td>{nullValueFunction(invitation.project.owner.username)}</td>
                    <td>
                        <Button variant="success" onClick={() => {this.AcceptInvite(invitation)}}>Akceptuj</Button>
                        <Button variant="danger" onClick={() => {this.DeclineInvite(invitation)}}>Odrzuć</Button>
                    </td>
                </tr>
            )
        }
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Projekt</th>
                        <th>Założyciel</th>
                        <th>Akcje</th>
                    </tr>
                </thead>
                <tbody>
                    {invitations}
                </tbody>
            </Table>
        )
    }

    AcceptInvite = (invitation) => {
        fetch(`/api/tasks/projects_members/${invitation.pk}/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')                 
            },
            body: JSON.stringify({
                invite: false
            })
            }).then(res => {
                if (res.status === 200) {
                    this.fetchInvitations()
                    this.props.updateHandler()
                    this.setState({
                        showSuccessInvitationAccept: true,
                        ShowSuccessInvitationDecline: false,
                    })
                } else if (res.status === 400){
                    res.json().then(data => {
                        console.log(data);
                    });
            } else if (res.status === 403){
                res.json().then(data => {
                    console.log(data);
                })
            }
                return res
            }).catch(error => {
                console.log(error);
            })
    }

    DeclineInvite = (invitation) => {
        fetch(`/api/tasks/projects_members/${invitation.pk}/`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')                 
            }
            }).then(res => {
                if (res.status === 204) {
                    this.fetchInvitations()
                    this.setState({
                        showSuccessInvitationAccept: false,
                        ShowSuccessInvitationDecline: true,
                    })
                } else if (res.status === 400){
                    res.json().then(data => {
                        console.log(data);
                    });
            } else if (res.status === 403){
                res.json().then(data => {
                    console.log(data);
                })
            }
                return res
            }).catch(error => {
                console.log(error);
            })
    }

    renderSuccessAcceptMessage = () => {
        if(this.state.showSuccessInvitationAccept === true){
            return <Alert variant="success">Pomyślnie zaakceptowano zaproszenie.</Alert>
        }
        return (<></>)
    }

    renderSuccessDeclineMessage = () => {
        if(this.state.ShowSuccessInvitationDecline === true){
            return <Alert variant="success">Pomyślnie odrzucono zaproszenie.</Alert>
        }
        return (<></>)
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={() => {this.props.closeHandler()}} size="lg" animation="true">
                <Modal.Header closeButton>
                  <Modal.Title>
                    <div className="project-invitations-modal-header-container">
                        <FontAwesomeIcon icon='envelope' size="2x"/>
                        <h4>Zaproszenia do projektów</h4>
                    </div>
                  </Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                        {this.renderSuccessAcceptMessage()}
                        {this.renderSuccessDeclineMessage()}
                        {this.renderInvitations()}
                    </Modal.Body>
                <Modal.Footer>
                </Modal.Footer>
              </Modal>
        )
    }
}

export default withRouter(ProjectInvitationsModal);
