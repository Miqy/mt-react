import React from 'react';
import "./project-list.css";
import {Button, Spinner} from 'react-bootstrap'
import ProjectCard from '../project-card/project-card'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { withRouter } from 'react-router-dom';


import ProjectCardForm from '../project-card-form/project-card-form';
import ProjectInvitationsModal from '../project-invitations-modal/project-invitations-modal';
import { AlertDismissible } from '../../views/login/login';


export class ProjectList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            projects: null,
            isInvitationsModalShow: false,
        }
    }

    fetchProjects(){
        fetch("/api/users/auth/user_info/").then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        user_data => {
                            fetch(`/api/tasks/projects/?member_or_owner=${user_data.pk}`).then(
                                res => {
                                    if(res.status === 200){
                                        res.json().then(
                                            data => {
                                                this.setState(
                                                    {
                                                        projects: data,
                                                        loading: false,
                                                        user: user_data
                                                    }
                                                )
                                            }
                                        )
                                    }else if (res.status === 401){
                                        this.props.history.push("/login")
                                    }else{
                                        console.log(res);
                                        this.props.history.push("/error")
                                    }
                                }
                            )
                        }
                    )
                }else{
                    this.props.history.push("/login")
                }
            }
        )
    }

    renderProjects(){
        let projects = []
        projects.push(this.renderNewProjectCard())
        for(let i = 0; i < this.state.projects.length; i++){
            let project = this.state.projects[i]
            projects.push(
                <ProjectCard key={i} name={project.name} description={project.description} id={project.pk}></ProjectCard>
            )
        }
        return projects   
    }

    renderNewProjectCard(){
        return (
            <ProjectCardForm key={-1}/>
        )
    }

    componentDidMount(){
        this.fetchProjects()
    }

    clearProjectMessage() {
        this.props.history.replace({ state: {actionModalMessage: ''} });
    }
  

    render() {
        if(this.state.loading === true){
            return (
                <div className="project-list-container">
                    <h1>Twoje projekty:</h1>
                    <Spinner animation="border" role="status" className="ml-auto">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </div>
            )
        }
        return (
            <div className="project-list-container">
                <div className='project-list-header-container'>
                    <div className="project-list-header">
                        <FontAwesomeIcon icon="briefcase" size='2x'></FontAwesomeIcon>
                        <h3>Twoje projekty</h3>
                    </div>
                    <div className="project-list-invitations">
                        <FontAwesomeIcon icon="envelope" size='2x'></FontAwesomeIcon>
                        <Button variant="light" onClick={() => {this.setState({isInvitationsModalShow: true})}}>Zaproszenia</Button>
                        <ProjectInvitationsModal 
                            show={this.state.isInvitationsModalShow} closeHandler={() => {this.setState({isInvitationsModalShow: false})} }
                            project={this.state.project} user={this.state.user} updateHandler={() => {this.fetchProjects()}}
                        />
                    </div>
                    
                </div>
                {this.props.location.state && this.props.location.state.actionModalMessage.length > 0 &&
                    <AlertDismissible variant="info" text={this.props.location.state.actionModalMessage} closeCallback={() => this.clearProjectMessage()}></AlertDismissible>
                }
                <ul className="project-cards-container">
                    {this.renderProjects()}
                </ul>
            </div>
        )
    }
}

export default withRouter(ProjectList);