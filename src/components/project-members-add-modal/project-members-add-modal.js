import React, { Component } from 'react';
import {Modal} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './project-members-add-modal.css'
import { withRouter } from 'react-router-dom';
import { InviteMemberForm } from '../invite-member-form/invite-member-form';



export class ProjectMembersAddModal extends Component {

    constructor(props){
        super(props)
        this.state = {
        }
    }


    render() {
        return (
            <Modal show={this.props.show} onHide={() => {this.props.closeHandler()}} animation="true">
                <Modal.Header closeButton>
                  <Modal.Title>
                    <div className="task-detail-title-container">
                        <FontAwesomeIcon icon='user' size="2x"/>
                        <h4>Zaproś do projektu</h4>
                    </div>
                  </Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                        <InviteMemberForm project={this.props.project}></InviteMemberForm>
                    </Modal.Body>
                <Modal.Footer>
                </Modal.Footer>
              </Modal>
        )
    }
}


export default withRouter(ProjectMembersAddModal)
