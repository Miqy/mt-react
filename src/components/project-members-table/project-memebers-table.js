import React, { Component } from 'react';
import {Table, Spinner} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';



export class ProjectMembersTable extends Component {

    constructor(props){
        super(props)
        this.state = {
            members: null,
            loadingMembers: true,
            invitedMembers: null,
            loadingInviteMembers: true
        }
        if (this.props.inviteAccept){
            this.fetchMembers()
        }else{
            this.fetchInvitedMembers()
        }
        
        
    }

    fetchMembers = () => {
        fetch(`/api/tasks/projects/${this.props.project.pk}/members`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    members: data,
                                    loadingMembers: false
                                }
                            )
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/")
                }else{
                    console.log(res);
                }
            }
        )
    }

    fetchInvitedMembers = () => {
        fetch(`/api/tasks/projects/${this.props.project.pk}/invited_members`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    invitedMembers: data,
                                    loadingInviteMembers: false
                                }
                            )
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/")
                }else{
                    console.log(res);
                }
            }
        )
    }

    renderMembers = (appendOwner, stateLoading, stateMembers) => {
        if (stateLoading === true){
            return (
                <Spinner animation="border" role="status" className="">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )
        }
        function nullValueFunction(value) {
            return value === null || value === "" ? "-" : value
        }
        let members = []
        for (let i=0; i < stateMembers.length; i++){
            let member = stateMembers[i]
            members.push(
                <tr key={i}>
                    <td>{nullValueFunction(member.first_name)}</td>
                    <td>{nullValueFunction(member.last_name)}</td>
                    <td>{nullValueFunction(member.username)}</td>
                    <td>{nullValueFunction(member.email)}</td>
                    {appendOwner && <td>{member.username === this.props.project.owner.username ? "Tak": "Nie"}</td>}
                </tr>
            )
        }
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Imię</th>
                        <th>Nazwisko</th>
                        <th>Nazwa użytkownika</th>
                        <th>E-mail</th>
                        {appendOwner && <th>Założyciel</th>}
                    </tr>
                </thead>
                <tbody>
                    {members}
                </tbody>
            </Table>
        )
    }

    render() {
        if (this.props.inviteAccept){
            return this.renderMembers(true, this.state.loadingMembers, this.state.members);
        }else{
            return this.renderMembers(false, this.state.loadingInviteMembers, this.state.invitedMembers);
        }
        
    }
}

export default withRouter(ProjectMembersTable);
