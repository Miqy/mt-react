import "./project-menu-modal.css";
import React, { Component } from 'react';
import {Modal} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { withRouter } from 'react-router-dom';
import ControlledTabs from '../controlled-tabs/controlled-tabs'
import ProjectMembersTable from '../project-members-table/project-memebers-table'
import ProjectSettings from '../project-settings/project-settings';
import { InviteMemberForm } from "../invite-member-form/invite-member-form";



export class ProjectMenuModal extends Component {

    constructor(props){
        super(props)
        this.state = {
        }
    }

    renderInvitedMembers() {
        return (
            <div className="invited-members-tab">
                <ProjectMembersTable project={this.props.project} inviteAccept={false}></ProjectMembersTable>
                <h4>Zaproś</h4>
                <InviteMemberForm project={this.props.project}></InviteMemberForm>
            </div>
        )
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={() => {this.props.closeHandler()}} size="lg" animation="true">
                <Modal.Header closeButton>
                  <Modal.Title>
                    <div className="task-detail-title-container">
                        <FontAwesomeIcon icon='bars' size="2x"/>
                        <h4>Menu</h4>
                    </div>
                  </Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                        <ControlledTabs 
                            tabsId='menuTabs'
                            names={['Członkowie', 'Zaproszeni użytkownicy', 'Ustawienia']}
                            components={
                                [<ProjectMembersTable project={this.props.project} inviteAccept={true}></ProjectMembersTable>,
                                this.renderInvitedMembers(),
                                <ProjectSettings project={this.props.project}></ProjectSettings>
                                ]
                                }>
                        </ControlledTabs>
                    </Modal.Body>
              </Modal>
        )
    }
}

export default withRouter(ProjectMenuModal);
