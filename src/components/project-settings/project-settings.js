import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import {Spinner} from 'react-bootstrap'
import DeleteProjectButton from '../delete-project-button/delete-project-button';
import LeaveProjectButton from '../leave-project-button/leave-project-button';


export class ProjectSettings extends Component {


    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            user: null
        }
    }

    componentDidMount() {
        this.getUserInfo();
    }

    getUserInfo() {
        fetch("/api/users/auth/user_info/").then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    user: data,
                                    loading: false
                                }
                            )
                        }
                    )
                }else{
                    this.props.history.push("/login")
                }
            }
        )
    }

    render() {
        if (this.state.loading){
            return (
                <Spinner animation="border" role="status" className="ml-auto">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )
        }
        let button = null;
        if (this.state.user.username === this.props.project.owner.username){
            button = <DeleteProjectButton project={this.props.project}></DeleteProjectButton>
        }else{
            button = <LeaveProjectButton variant="danger" project={this.props.project} user={this.state.user}>Opuść projekt</LeaveProjectButton>
        }
        return (
            <div className='project-settings-container'>
                {button}
            </div>
        )
    }
}

export default withRouter(ProjectSettings);
