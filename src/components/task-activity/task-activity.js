import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import Cookies from 'js-cookie';
import Moment from 'react-moment';
import 'moment/locale/pl';
import ReactMarkdown from 'react-markdown'
import breaks from 'remark-breaks'
import ReactDiffViewer from 'react-diff-viewer'
import {Form, Button} from 'react-bootstrap'
import './task-activity.css'


export class TaskActivity extends Component {

    constructor(props){
        super(props);
        this.state = {
            noteEditFormOpen: false,
            noteInput: this.props.activity.value
        }
    }

    handleNoteEditSubmit = (event) => {
        event.preventDefault();
        fetch(`/api/tasks/task_activities/${this.props.activity.pk}/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')                 
            },
            body: JSON.stringify({
                value: this.state.noteInput,
            })
            }).then(res => {
                if (res.status === 200) {
                    this.setState({
                        noteEditFormOpen: false
                    })
                    this.props.reloadParentTaskDetail()
                } else if (res.status === 400){
                    res.json().then(data => {
                    console.log(data);
                    });
            } else if (res.status === 403){
                res.json().then(data => {
                    console.log(data);
                })
            }
                return res
            }).catch(error => {
                console.log(error);
            })
    }

    render() {
        let activity = this.props.activity
        let note_content = null
        if(this.state.noteEditFormOpen === false){
            note_content = <ReactMarkdown source={activity.content} plugins={[breaks]}/>
        }else{
            note_content = (
                <Form onSubmit={this.handleNoteEditSubmit}>
                    <Form.Group controlId="noteInput">
                        <Form.Control
                            as="textarea" rows="3" placeholder="Dodaj notatkę"
                            value={this.state.noteInput} onChange={
                                (event) => {
                                    this.setState({noteInput: event.target.value})
                                }
                            }
                            />
                    <Button variant="success" type="submit" disabled={this.state.noteInput.length === 0}>
                        Zapisz
                    </Button>
                    <Button variant="danger" type="submit" onClick={() => {this.setState({noteEditFormOpen: false, noteInput: this.props.activity.value})}}>
                        Anuluj
                    </Button>
                    </Form.Group>
                </Form>
            )
        }
        return (
            <li key={activity.pk}>
                <p>Uaktualnione przez <b>{activity.user.username}</b> <i><Moment locale="pl" fromNow>{activity.create_date}</Moment></i></p>
                {activity.field_type === 'note' && (
                    <div className="task-activity-note-container">
                        Dodano <b>Notatkę</b>.
                        {activity.can_edit && <Button variant="light" onClick={() => {this.setState({noteEditFormOpen: true})}}>Edytuj</Button>}
                    </div>
                )}
                {note_content}
                { activity.field_type === 'description' && <ReactDiffViewer
                    oldValue={activity.prev_value === null ? '': activity.prev_value}
                    newValue={activity.value === null ? '' : activity.value}
                    splitView={false}
                    codeFoldMessageRenderer={() => {return 'Wyświetl ukryte linie'}}
                />}
            </li>
        )
    }
}

export default withRouter(TaskActivity);
