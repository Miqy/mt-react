import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Form, Button} from 'react-bootstrap'
import Cookies from 'js-cookie';
import './task-card-form.css'


export class TaskCardForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isFormOpen: false,
            title: "",
            isRequested: false,
        }
    }

    openForm = () => {
        if(this.state.isFormOpen === false){
            this.setState({
                isFormOpen: true
            })
        }
    }

    validateForm() {
        return this.state.title.length > 0 && !this.isRequested;
    }

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        if (this.state.isRequested === true){
            return
        }
        this.setState({
            isRequested: true
        });
        event.preventDefault();
        fetch("/api/tasks/tasks/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify({
                title: this.state.title,
                status: this.props.status.pk,
                project: this.props.project.pk
            })
            }).then(res => {
                this.setState({
                    isRequested: false
                });
                if (res.status === 201) {
                    this.props.updateParent()
                    this.setState({
                        isFormOpen: false,
                        title: "",
                        isRequested: false
                    })
                } else if (res.status === 400){
                    res.json().then(data => {
                    console.log(data);
                    });
            } else if (res.status === 403){
                res.json().then(data => {
                    console.log(data);
                })
            }
                return res
            }).catch(error => {
                this.setState({
                    isRequested: false
                });
                console.log(error);
            })
    }


    render() {
        if (this.state.isFormOpen === true && !this.state.isRequested){
            return (
                <div className="task-card-form-container">
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group controlId="title">
                            <Form.Control type="text" placeholder="Wprowadź nazwę zadania"
                                value={this.state.title}
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={!this.validateForm()}>
                            Utwórz
                        </Button>
                    </Form>
                </div>
            )
        }
        return (
            <div className="task-card-form-container" onClick={this.openForm}>
                <FontAwesomeIcon icon="plus-circle" size='1x'/>
                <p>Utwórz zadanie</p>
            </div>
        )
    }
}

export default TaskCardForm
