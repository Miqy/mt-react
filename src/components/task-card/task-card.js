import React, { Component } from 'react'
import "./task-card.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export class TaskCard extends Component {
    
    render() {
        return (
            <div>
                <div className="task-card-container" onClick={() => {this.props.taskModalShowCallback(this.props.task.pk)}}>
                    <div className="task-card-prop-info"><FontAwesomeIcon icon='book'/><p>{this.props.task.title}</p></div>
                    <div className="task-card-prop-info task-card-prop-bottom"><FontAwesomeIcon icon='user'/><p>{this.props.task.assign_to !== null ? this.props.task.assign_to.username : "---"}</p></div>
                </div>
            </div>
        )
    }
}

export default TaskCard
