import React, { Component } from 'react'
import {Modal, Form, Button, Spinner} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './task-detail-modal.css'
import Cookies from 'js-cookie';
import Moment from 'react-moment';
import 'moment/locale/pl';
import ReactMarkdown from 'react-markdown'
import breaks from 'remark-breaks'
import { TaskActivity } from '../task-activity/task-activity';
import { Errors } from "../../views/login/login"




export class TaskDetailModal extends Component {

    constructor(props){
        super(props);
        let task = this.props.task
        if (task.description === null){
            task.description = ""
        }
        this.state = {
            task: task,
            loadingTask: true,
            isDescriptionFormOpen: false,
            isStatusFormOpen: false,
            isProgressFormOpen: false,
            isAssignedFormOpen: false,
            descriptionInput: task.description,
            statusInput: task.status,
            progressInput: task.progress,
            assignedInput: task.assign_to,
            noteInput: "",
            errors: null,
        }
    }


    UNSAFE_componentWillReceiveProps() {
        this.forceUpdate();
    }

    fetchTaskDetail = () => {
        fetch(`/api/tasks/tasks/${this.props.task.pk}/`,).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            let task = data
                            if (task.description === null){
                                task.description = ""
                            }
                            this.setState(
                                {
                                    task: task,
                                    loadingTask: false
                                }
                            )
                            
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/login")
                }
            }
        )
    }

    handleClose = () => {
        this.props.closeHandler()
    }

    handleSubmit = (event, patchBody, successCallback) => {
        event.preventDefault();
        fetch(`/api/tasks/tasks/${this.state.task.pk}/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')                 
            },
            body: JSON.stringify(patchBody)
            }).then(res => {
                if (res.status === 200) {
                    successCallback()
                    this.fetchTaskDetail()
                } else if (res.status === 400){
                    res.json().then(data => {
                        console.log(data);
                    });
            } else if (res.status === 403){
                res.json().then(data => {
                    this.setState({errors: data});
                })
            }
                return res
            }).catch(error => {
                console.log(error);
            })
    }
    handleDescriptionSubmit = event => {
        this.handleSubmit(event, {description: this.state.descriptionInput}, () => {
            let task = this.state.task
            task.description = this.state.descriptionInput
            this.setState({
                isDescriptionFormOpen: false,
                task: task
            })
        })
    }

    handleStatusSubmit = event => {
        this.handleSubmit(event, {status: this.state.statusInput.pk}, () => {
            let task = this.state.task
            task.status = this.state.statusInput
            this.setState({
                isStatusFormOpen: false,
                task: task
            })
        })
        this.props.fetchProjectCallback()
    }

    handleAssignedSubmit = event => {
        this.handleSubmit(event, {assign_to: this.state.assignedInput === 0 ? null: this.state.assignedInput}, () => {
            let task = this.state.task
            task.assign_to = this.state.assignedInput
            this.setState({
                isAssignedFormOpen: false,
                task: task
            })
        })
    }

    handleProgressSubmit = event => {
        this.handleSubmit(event, {progress: this.state.progressInput}, () => {
            let task = this.state.task
            task.progress = this.state.progressInput
            this.setState({
                isProgressFormOpen: false,
                task: task,
            })
        })
    }

    handleDescriptionFormChange = (event) => {
        this.setState({
            descriptionInput: event.target.value,
        }); 
    }

    handleNoteSubmit = (event) => {
        event.preventDefault();
        fetch(`/api/tasks/task_activities/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')                 
            },
            body: JSON.stringify({
                value: this.state.noteInput,
                task: this.state.task.pk
            })
            }).then(res => {
                if (res.status === 201) {
                    this.setState({
                        noteInput: "",
                    })
                    this.fetchTaskDetail()
                } else if (res.status === 400){
                    res.json().then(data => {
                    console.log(data);
                    });
            } else if (res.status === 403){
                res.json().then(data => {
                    console.log(data);
                })
            }
                return res
            }).catch(error => {
                console.log(error);
            })
    }

    handleProgressFormChange = (event) => {
        this.setState({
            progressInput: event.target.value,
        }); 
    }

    handleStatusFormChange = (event) => {
        let obj = this.props.project.statuses.find(obj => {
            return +obj.pk === +event.target.value
        })
        this.setState({
            statusInput: obj,
        });
    }

    handleAssignedFormChange = (event) => {
        let obj = this.props.project.members.find(obj => {
            return +obj.pk === +event.target.value
        })
        this.setState({
            assignedInput: typeof obj === 'undefined' ? 0 : obj.pk,
        });
    }

    renderDescriptionForm = () => {
        if (this.state.isDescriptionFormOpen) {
            return (
                <Form onSubmit={this.handleDescriptionSubmit}>
                    <Form.Group controlId="description">
                        <Form.Control
                            as="textarea" rows="6" placeholder="Wprowadź opis zadania"
                            value={this.state.descriptionInput} onChange={this.handleDescriptionFormChange}
                            />
                    <Button variant="success" type="submit">
                        Zapisz
                    </Button>
                    <Button variant="danger" onClick={() => {this.setState({isDescriptionFormOpen: false, descriptionInput: this.state.task.description})}}>
                        Anuluj
                    </Button>
                    </Form.Group>
                </Form>
            )
        }
        return (
            <ReactMarkdown source={this.state.task.description === null ? '' : this.state.task.description} plugins={[breaks]} />
        )
    }
    
    renderStatusForm = () => {
        if (this.state.isStatusFormOpen) {
            let statuses = []
            for (let i = 0; i < this.props.project.statuses.length; i++){
                let status = this.props.project.statuses[i]
                statuses.push(<option key={i} value={status.pk}>{status.name}</option>)
            }
            return (
                <Form onSubmit={this.handleStatusSubmit} className="task-detail-status-form">
                    <Form.Group controlId="status">
                        <Form.Control
                            as="select" placeholder="Zmień status"
                            value={this.state.statusInput.pk} onChange={this.handleStatusFormChange}
                            >
                            {statuses}
                        </Form.Control>
                        <Button variant="success" type="submit" >
                            Zapisz
                        </Button>
                        <Button variant="danger" onClick={() => {this.setState({isStatusFormOpen: false, statusInput: this.state.task.status})}}>
                            Anuluj
                        </Button>
                    </Form.Group>
                    
                </Form>
            )
        }
        return (
            <>
                <p>{this.state.task.status.name}</p>
            </>
        )
    }

    renderProgressForm = () => {
        if (this.state.isProgressFormOpen) {
            let progress_list = []
            for (let i = 0; i < 11; i++){
                progress_list.push(<option key={i} value={i * 10}>{i * 10}%</option>)
            }
            return (
                <Form onSubmit={this.handleProgressSubmit} className="task-detail-status-form">
                    <Form.Group controlId="progress">
                        <Form.Control
                            as="select" placeholder="Zmień postęp"
                            value={this.state.progressInput} onChange={this.handleProgressFormChange}
                            >
                            {progress_list}
                        </Form.Control>
                        <Button variant="success" type="submit" >
                            Zapisz
                        </Button>
                        <Button 
                            variant="danger" 
                            onClick={() => {this.setState({isProgressFormOpen: false, progressInput: this.state.task.progress})}}
                        >
                            Anuluj
                        </Button>
                    </Form.Group>
                    
                </Form>
            )
        }
        return (
            <>
                <p>{this.state.task.progress}%</p>
            </>
        )
    }

    renderAssigned = (selectedObj) => {
        var textStr = "---"
        if(selectedObj === null){
            return textStr
        }
        let obj = this.props.project.members.find(obj => {
            return +obj.pk === +selectedObj.pk
        })
        if (typeof obj === 'undefined'){
            return textStr
        }
        return obj.username
    }

    renderAssignedForm = () => {
        if (this.state.isAssignedFormOpen) {
            let members = []
            members.push(<option key={-1} value={0}>---</option>)
            for (let i = 0; i < this.props.project.members.length; i++){
                let member = this.props.project.members[i]
                members.push(<option key={i} value={member.pk}>{member.username}</option>)
            }
            return (
                <Form onSubmit={this.handleAssignedSubmit} className="task-detail-assigned-form">
                    <Form.Group controlId="members">
                        <Form.Control
                            as="select" placeholder="Przypisz użytkownika"
                            value={this.state.assignedInput === null ? 0 : this.state.assignedInput} onChange={this.handleAssignedFormChange}
                            >
                            {members}
                        </Form.Control>
                        <Button variant="success" type="submit" >
                            Zapisz
                        </Button>
                        <Button variant="danger" onClick={() => {this.setState({isAssignedFormOpen: false, assignedInput: this.state.task.assign_to})}}>
                            Anuluj
                        </Button>
                    </Form.Group>
                    
                </Form>
            )
        }
        return (
            <>
                <p>{this.renderAssigned(this.state.task.assign_to)}</p>
            </>
        )
    }

    renderActivities = () => {
        let activities = []
        activities.push(
            <Form key={-1}onSubmit={this.handleNoteSubmit}>
                <Form.Group controlId="noteInput">
                    <Form.Control
                        as="textarea" rows="3" placeholder="Dodaj notatkę"
                        value={this.state.noteInput} onChange={
                            (event) => {
                                this.setState({noteInput: event.target.value})
                            }
                        }
                        />
                <Button variant="success" type="submit" disabled={this.state.noteInput.length === 0}>
                    Dodaj
                </Button>
                </Form.Group>
            </Form>
        )
        if (typeof this.state.task.activities === 'undefined'){
            return activities
        }
        for(let i = 0; i < this.state.task.activities.length; i++){
            let item = this.state.task.activities[i]
            activities.push(<TaskActivity key={item.pk} activity={item} reloadParentTaskDetail={this.fetchTaskDetail}></TaskActivity>)
        }
        return activities
    }

    openDescriptionForm = () => {
        this.setState({
            isDescriptionFormOpen: true
        })
    }

    openStatusForm = () => {
        this.setState({
            isStatusFormOpen: true
        })
    }

    openAssignedForm = () => {
        this.setState({
            isAssignedFormOpen: true
        })
    }

    openProgressForm = () => {
        this.setState({
            isProgressFormOpen: true
        })
    }


    render() {
        let modalBody = (
            <div className="task-detail-modal-body">
                <Errors errors={this.state.errors === null ? [] : [this.state.errors['detail']]}></Errors>
                <div className='task-detail-modal-header-fields'>
                    <div className="task-detail-moadal-left-body">
                        <div className="task-detail-description-container">
                            <div className="task-detail-description-header">
                                <FontAwesomeIcon icon='calendar' size="2x" />
                                <h4>Data utworzenia</h4>
                            </div>
                            <div className="task-detail-description-content">
                                <Moment format="YYYY/MM/DD HH:mm">{this.state.task.create_date}</Moment>
                            </div>
                        </div>
                        <div className="task-detail-description-container">
                            <div className="task-detail-description-header">
                                <FontAwesomeIcon icon='stream' size="2x" />
                                <h4>Status</h4>
                                <Button variant="light" onClick={this.openStatusForm}>Edytuj</Button>
                            </div>
                            <div className="task-detail-description-content">
                                {this.renderStatusForm()}
                            </div> 
                        </div>
                    </div>
                    <div className="task-detail-moadal-right-body">
                        <div className="task-detail-description-container">
                            <div className="task-detail-description-header">
                                <FontAwesomeIcon icon='tasks' size="2x" />
                                <h4>Postęp</h4>
                                <Button variant="light" onClick={this.openProgressForm}>Edytuj</Button>
                            </div>
                            <div className="task-detail-description-content">
                                {this.renderProgressForm()}
                            </div> 
                        </div>
                        <div className="task-detail-description-container">
                            <div className="task-detail-description-header">
                                <FontAwesomeIcon icon='user' size="2x" />
                                <h4>Przypisany do</h4>
                                <Button variant="light" onClick={this.openAssignedForm}>Edytuj</Button>
                            </div>
                            <div className="task-detail-description-content">
                                {this.renderAssignedForm()}
                            </div> 
                        </div>
                    </div>
                </div>
                <div className="task-detail-description-block">
                    <div className="task-detail-description-container">
                        <div className="task-detail-description-header">
                            <FontAwesomeIcon icon='edit' size="2x" />
                            <h4>Opis</h4>
                            <Button variant="light" onClick={this.openDescriptionForm}>Edytuj</Button>
                        </div>
                        <div className="task-detail-description-content">
                            {this.renderDescriptionForm()}
                        </div>
                    </div>
                </div>
                <div className="task-detail-description-container">
                    <div className="task-detail-description-header">
                        <FontAwesomeIcon icon='history' size="2x" />
                        <h4>Aktywności</h4>
                    </div>
                    <div className="task-detail-description-content">
                        <ul>
                            {this.renderActivities()}
                        </ul>
                    </div> 
                </div>
            </div>
        )
        let spinner = (
            <Spinner animation="border" role="status" className="ml-auto">
                <span className="sr-only">Loading...</span>
            </Spinner>
        )
        let content = this.state.loadingTask === true ? spinner : modalBody
        return (
            <div className="task-detail-modal-container">
              <Modal show={this.props.show} onHide={this.handleClose} size="lg" animation="true" onShow={this.fetchTaskDetail} scrollable={true}>
                <Modal.Header closeButton>
                  <Modal.Title>
                    <div className="task-detail-title-container">
                        <FontAwesomeIcon icon='book' size="2x"/>
                        <h4>#{this.state.task.pk} {this.state.task.title}</h4>
                    </div>
                  </Modal.Title>
                </Modal.Header>
                    <Modal.Body>
                        {content}
                    </Modal.Body>
                <Modal.Footer>
                </Modal.Footer>
              </Modal>
            </div>
          );
    }
}

export default TaskDetailModal
