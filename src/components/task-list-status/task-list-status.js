import React, { Component } from 'react';
import './task-list-status.css'
import { Spinner } from 'react-bootstrap';
import TaskCard from '../task-card/task-card';
import TaskCardForm from '../task-card-form/task-card-form';
import { withRouter } from 'react-router-dom';


export class TaskListStatus extends Component {

    constructor(props){
        super(props)
        this.state = {
            loading: true,
            tasks: null,
        }
    }

    UNSAFE_componentWillReceiveProps() {
        this.fetchTasks();
    }

    componentDidMount(){
        this.fetchTasks();
    }

    updateHandler = () => {
        // update handler from child component
        this.fetchTasks()
        this.props.updateParent()
    }

    fetchTasks = () => {
        fetch(`/api/tasks/tasks?ordering=-pk&project=${this.props.project.pk}&status=${this.props.status.pk}`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    tasks: data,
                                    loading: false
                                }
                            )
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/login")
                }
            }
        )
    }

    renderTasks = () => {
        let tasks = []
        tasks.push(<TaskCardForm key={-1} status={this.props.status} project={this.props.project} updateParent={this.updateHandler}/>)
        for(let i = 0; i< this.state.tasks.length; i++){
            tasks.push(<TaskCard key={i} task={this.state.tasks[i]} project={this.props.project} taskModalShowCallback={this.props.taskModalShowCallback}/>)
        }
        return tasks
    }

    render() {
        if (this.state.loading === true){
            return (
                <Spinner animation="border" role="status" className="">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )
        }else{
            return (
                <div className="task-list-status-container">
                    <h4>{this.props.status.name}</h4>
                    <div className="task-list-container">
                        {this.renderTasks()}
                    </div>
                </div>
            )
        }
    }
}

export default withRouter(TaskListStatus)
