import React from 'react';
import { Route } from 'react-router-dom';


export function requireAuth(history, component){
    /** Function get history for redirect and component to return.
     * If user is not authenticated redirect to login page.
     * 
     */
    fetch("/api/users/auth/login_status/").then(
        res => {
        res.json().then(
            data => {
            if (data['authenticated'] === false) {
                if( typeof history === 'undefined'){
                    return
                }
                history.push('/login')
            }
            },
            error => {
                history.push('/login')
            }
        )
        }
    )
    return component
}


export class RequireAuthRoute extends React.Component {
    /**
     * Component redirect to comoonent passed in props if user is not logged in.
     */

    requireAuth = requireAuth

    render(){
      return (
        <Route exact path={this.props.path} render={({history}) => {return this.requireAuth(history, this.props.component)}}/>
      )
    }
  
}

export class LoginRedirectRoute extends React.Component {
    /**
     * Component redirect to component home if user is logged in and want to go to login componenet.
     */

    redirectLogin(history, component){
        /** Function get history for redirect and component to return.
         * If user is authenticated redirect to home.
         * 
         */
        fetch("/api/users/auth/login_status/").then(
            res => {
            res.json().then(
                data => {
                    if (data['authenticated'] === true) {
                        history.push('/home')
                    }
                },
            )
            }
        )
        return component
    }

    render(){
      return (
        <Route exact path={this.props.path} render={({history}) => {return this.redirectLogin(history, this.props.component)}}/>
      )
    }
  
}