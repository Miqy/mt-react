import React from 'react';
import ProjectList from '../../components/project-list/project-list';
import NavbarComponent from '../../components/navbar/navbar'

export class Home extends React.Component {
    render() {
        return (
            <div className="home-container">
                <NavbarComponent></NavbarComponent>
                <ProjectList/>
            </div>
        )
    }
}