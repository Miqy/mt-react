import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, FormGroup, FormControl, FormLabel, Alert } from "react-bootstrap";
import {useState} from 'react'
// import { GoogleLogin } from 'react-google-login';
import "./login.css";


// const responseGoogle = (response) => {
//   console.log(response);
// }

class Login extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        username: "",
        password: "",
        errors: [],
      };
    }
  
    validateForm() {
      return this.state.username.length > 0 && this.state.password.length > 0;
    }
  
    handleChange = event => {
      this.setState({
        [event.target.id]: event.target.value
      });
    }
  
    handleSubmit = event => {
      event.preventDefault();
      fetch("/api/users/auth/login/", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',                  
      },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
        })
      }).then(res => {
        if (res.status === 200) {
          this.props.history.push('/')
          
        } else if (res.status === 400){
          res.json().then(data => {
            this.setState({
              errors: data['non_field_errors']
            });
          });
        } else if (res.status === 403){
          res.json().then(data => {
              this.setState({
                errors: data['detail']
              })
            }
          )
        } else{
          this.props.history.push('/error')
        }
        return res
      }).catch(error => {
        
      })
    }

    clearRegisterMessage() {
      this.props.history.replace({ state: {isRegistered: false} });
    }

    render() {
      return (
        <div>
          <div className="logo"></div>
          <div className="login-block">
            <h1>Zaloguj się do aplikacji Meety</h1>
            <form onSubmit={this.handleSubmit}>
              <Errors errors={this.state.errors}></Errors>
              {this.props.location.state && this.props.location.state.isRegistered &&
                <AlertDismissible variant="info" text='Pomyślnie zarejestrowano. Możesz teraz się zalogować.' closeCallback={() => this.clearRegisterMessage()}></AlertDismissible>
              }
              <div className="login-form-block">
                <FormGroup controlId="username" size="large">
                  <FormLabel>Nazwa użytkownika</FormLabel>
                  <FormControl
                    autoFocus
                    type="text"
                    value={this.state.username}
                    onChange={this.handleChange}
                  />
                </FormGroup>
                <FormGroup controlId="password" size="large">
                  <FormLabel>Hasło</FormLabel>
                  <FormControl
                    value={this.state.password}
                    onChange={this.handleChange}
                    type="password"
                  />
                </FormGroup>
                <Button
                  block
                  size="large"
                  disabled={!this.validateForm()}
                  type="submit"
                >
                  Zaloguj się
                </Button>
              </div>
            </form>
            <p>Nie masz konta?
              <a href="/register"> Zarejestruj się</a>
            </p>
            {/* # TODO: Google login */}
            {/* <GoogleLogin
              clientId="235502980856-7d0v6u5fdglb4ot06ka7neqnudrefmd4.apps.googleusercontent.com"
              buttonText="Zaloguj się"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={'single_host_origin'}
            /> */}
          </div>
        </div>
      );
    }
  }

export function Errors(props){
  if (props.errors.length){
    return <Alert variant='danger'>{props.errors}</Alert>
  }
  return <div></div>
}

export function AlertDismissible(props) {
  const [show, setShow] = useState(true);

  if (show) {
    return (
      <Alert variant={props.variant} onClose={() => {setShow(false); props.closeCallback()}} dismissible>
        <p>
          {props.text}
        </p>
        
      </Alert>
    );
  }
  return (<></>)
}

export default withRouter(Login);