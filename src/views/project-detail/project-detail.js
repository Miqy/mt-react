import React, { Component } from 'react'
import NavbarComponent from '../../components/navbar/navbar'
import { Spinner, Button } from 'react-bootstrap';
import './project-detail.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import TaskListStatus from '../../components/task-list-status/task-list-status';
import TaskDetailModal from '../../components/task-detail-modal/task-detail-modal'
import ProjectMembersModal from '../../components/project-members-modal/project-members-modal';
import ProjectMembersAddModal from '../../components/project-members-add-modal/project-members-add-modal';
import ProjectMenuModal from '../../components/project-menu-modal/project-menu-modal';
import { withRouter } from 'react-router-dom';


export class ProjectDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            project: null,
            tasks: null,
            loading: true,
            loadingTasks: true,
            isMembersListModalShow: false,
            isMembersAddModalShow: false,
            isMenuOpen: false,
        }
    }

    componentDidMount() {
        this.fetchProjectDetail();
        this.fetchTasks();
    }

    updateProjectAndTasks = () => {
        this.fetchProjectDetail()
        this.fetchTasks()
    }

    fetchProjectDetail = () => {
        fetch(`/api/tasks/projects/${this.props.id}`).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    project: data,
                                    loading: false
                                }
                            )
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/login")
                }else if (res.status === 404){
                    this.props.history.push("/home")
                }
            }
        )
    }

    fetchTasks = () => {
        fetch(`/api/tasks/tasks/?ordering=-pk&project=${this.props.id}`,).then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            for(let task of data){
                                task.isModalShowed = false
                            }
                            this.setState(
                                {
                                    tasks: data,
                                    loadingTasks: false
                                }
                            )
                            
                        }
                    )
                }else if (res.status === 401){
                    this.props.history.push("/login")
                }
            }
        )
    }

    renderStatuses = () => {
        let statuses = []

        for (let i=0; i< this.state.project.statuses.length; i++){
            statuses.push(<TaskListStatus key={i} status={this.state.project.statuses[i]} project={this.state.project} taskModalShowCallback={this.handleShow} updateParent={this.updateProjectAndTasks}/>)
        }
        return statuses
    }

    handleClose = (taskId) => {
        let tasks = this.state.tasks
        let obj = tasks.find(obj => {
            return +obj.pk === +taskId
        })
        obj.isModalShowed = false
        this.setState({
            tasks: tasks
        })
    }

    handleShow = (taskId) => {
        let tasks = this.state.tasks
        let obj = tasks.find(obj => {
            return +obj.pk === +taskId
        })
        obj.isModalShowed = true
        this.setState({
            tasks: tasks
        })
    }

    renderTaskModals = () => {
        let modals = []

        for (let i=0; i< this.state.tasks.length; i++){
            modals.push(
                <TaskDetailModal
                    key={i} task={this.state.tasks[i]} 
                    show={this.state.tasks[i].isModalShowed} closeHandler={() => {this.handleClose(this.state.tasks[i].pk) } }
                    project={this.state.project} fetchProjectCallback={this.fetchProjectDetail}
                 />
            )
        }
        return modals
    }

    render() {
        if(this.state.loading === true || this.state.loadingTasks === true){
            return (
                <div className='project-detail-container'>
                <NavbarComponent/>
                <Spinner animation="border" role="status" className="">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            </div>
            )
        }
        return (
            <div className='project-detail-container'>
                <NavbarComponent/>
                <div className="project-detail-header-container">
                    <div className="project-detail-header-left">
                        <div className="project-detail-header">
                            <FontAwesomeIcon icon="briefcase" size='2x'></FontAwesomeIcon>
                            <h3>{this.state.project.name.substring(0, 35)}</h3>
                        </div>
                        <div className="project-detail-members">
                            <FontAwesomeIcon icon="user" size='2x'></FontAwesomeIcon>
                            <Button variant="light" onClick={() => {this.setState({isMembersListModalShow: true})}}>Członkowie</Button>
                            <Button variant="success"onClick={() => {this.setState({isMembersAddModalShow: true})}}>Zaproś</Button>
                        </div>
                    </div>
                    <div className="project-detail-header-right">
                        <div className='project-detail-header-settings'>
                            <Button variant="light" onClick={() => this.setState({isMenuOpen: true})}>
                            <FontAwesomeIcon icon="bars" size='2x'></FontAwesomeIcon>
                            </Button>
                        </div>
                    </div> 
                    
                    <ProjectMembersModal 
                        show={this.state.isMembersListModalShow} closeHandler={() => {this.setState({isMembersListModalShow: false})} }
                        project={this.state.project}
                    />
                    <ProjectMembersAddModal
                        show={this.state.isMembersAddModalShow} closeHandler={() => {this.setState({isMembersAddModalShow: false})} }
                        project={this.state.project}
                    />
                    <ProjectMenuModal
                        show={this.state.isMenuOpen} closeHandler={() => {this.setState({isMenuOpen: false})}} project={this.state.project}
                    ></ProjectMenuModal>
                    

                </div>
                <div className="project-detail-statuses-container">
                    {this.renderStatuses()}
                    {this.renderTaskModals()}
                </div>
            </div>
        )
    }
}

export default withRouter(ProjectDetail)