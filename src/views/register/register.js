import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import { Errors } from "../login/login"
import "./register.css";

export class Register extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          username: "",
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          password2: "",
          errors: [],
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        fetch("/api/users/auth/users/", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',                  
        },
          body: JSON.stringify({
            username: this.state.username,
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            email: this.state.email,
            password: this.state.password,
          })
        }).then(res => {
          if (res.status === 201) {
            this.props.history.push({
              pathname: '/login',
              state: {isRegistered: true}
            })
            
          } else if (res.status === 400){
            res.json().then(data => {
              this.setState({
                errors: data
              });
            });
          } else if (res.status === 403){
            res.json().then(data => {
                this.setState({
                  errors: data['detail']
                })
              }
            )
          } else {
            this.props.history.push('/error')
          }
          return res
        }).catch(error => {
        })
    }

    render() {
        return (
            <div>
          <div className="logo"></div>
          <div className="login-block register-block">
            <h1>Rejestracja</h1>
            <form onSubmit={this.handleSubmit}>
              <Errors errors={typeof(this.state.errors['non_field_errors']) === 'undefined' ? [] : this.state.errors['non_field_errors']}></Errors>
              <FormGroup controlId="username" size="large">
                <FormLabel>Nazwa użytkownika*</FormLabel>
                <FormControl
                required
                  autoFocus
                  type="text"
                  value={this.state.username}
                  onChange={this.handleChange}
                  isInvalid={this.state.errors.hasOwnProperty("username")}
                />
                <FormControl.Feedback type="invalid">
                  {this.state.errors['username']}
                </FormControl.Feedback>
              </FormGroup>
              <FormGroup controlId="firstName" size="large">
                <FormLabel>Imię</FormLabel>
                <FormControl
                  type="text"
                  value={this.state.firstName}
                  onChange={this.handleChange}
                  isInvalid={this.state.errors.hasOwnProperty("first_name")}
                />
                <FormControl.Feedback type="invalid">
                  {this.state.errors['first_name']}
                </FormControl.Feedback>
              </FormGroup>
              <FormGroup controlId="lastName" size="large">
                <FormLabel>Nazwisko</FormLabel>
                <FormControl
                  type="text"
                  value={this.state.lastName}
                  onChange={this.handleChange}
                  isInvalid={this.state.errors.hasOwnProperty("last_name")}
                />
                <FormControl.Feedback type="invalid">
                  {this.state.errors['last_name']}
                </FormControl.Feedback>
              </FormGroup>
              <FormGroup controlId="email" size="large">
                <FormLabel>Email*</FormLabel>
                <FormControl
                  type="text"
                  required
                  value={this.state.email}
                  onChange={this.handleChange}
                  isInvalid={this.state.errors.hasOwnProperty("email")}
                />
                <FormControl.Feedback type="invalid">
                  {this.state.errors['email']}
                </FormControl.Feedback>
              </FormGroup>
              <FormGroup controlId="password" size="large">
                <FormLabel>Hasło*</FormLabel>
                <FormControl
                  value={this.state.password}
                  onChange={this.handleChange}
                  type="password"
                  required
                  isInvalid={this.state.errors.hasOwnProperty("password")}
                />
                <FormControl.Feedback type="invalid">
                  {this.state.errors['password']}
                </FormControl.Feedback>
              </FormGroup>
              <FormGroup controlId="password2" size="large">
                <FormLabel>Potwierdź hasło*</FormLabel>
                <FormControl
                  value={this.state.password2}
                  onChange={this.handleChange}
                  type="password"
                  required
                  isInvalid={this.state.password !== this.state.password2}
                />
                <FormControl.Feedback type="invalid">
                  Podane hasła różnią się
                </FormControl.Feedback>
              </FormGroup>
              <Button
                block
                size="large"
                type="submit"
              >
                Zarejestruj się
              </Button>
              <br/>
              <a href="/login"> Wróć na stronę logowania</a>
            </form>
          </div>
        </div>
        )
    }
}

export default withRouter(Register);
