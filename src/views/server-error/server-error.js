import React from 'react';
import { withRouter } from 'react-router-dom';


export class ServerError extends React.Component {
    render() {
        return (
            <div className="server-error-container">
                <h1>Wystąpił nieoczekiwany problem.</h1>
                <a href="/home"> Wróć na strone domową.</a>
            </div>
        )
    }
}

export default withRouter(ServerError);