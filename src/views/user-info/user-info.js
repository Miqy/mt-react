import React from 'react';
import NavbarComponent from '../../components/navbar/navbar'
import { withRouter } from 'react-router-dom';
import {Spinner, ListGroup} from 'react-bootstrap';
import "./user-info.css";
import Moment from 'react-moment';


export class UserInfo extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            user: null
        }
    }

    getUserInfo() {
        fetch("/api/users/auth/user_info/").then(
            res => {
                if(res.status === 200){
                    res.json().then(
                        data => {
                            this.setState(
                                {
                                    user: data,
                                    loading: false
                                }
                            )
                        }
                    )
                }else{
                    this.props.history.push("/login")
                }
            }
        )
    }

    componentDidMount() {
        this.getUserInfo()
    }

    render() {
        if(this.state.loading === true){
            return (
                <div className="user-info-center">
                    <NavbarComponent></NavbarComponent>
                    <Spinner animation="border" role="status" className="">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </div>
            )
        }else{
            return (
                <div className='user-info-container'>
                    <NavbarComponent></NavbarComponent>
                    <div className="user-info-center">
                        <ListGroup>
                            <ListGroup.Item>Nazwa użytkownika: {this.state.user.username}</ListGroup.Item>
                            <ListGroup.Item>Imie: {this.state.user.first_name}</ListGroup.Item>
                            <ListGroup.Item>Nazwisko: {this.state.user.last_name}</ListGroup.Item>
                            <ListGroup.Item>Email: {this.state.user.email}</ListGroup.Item>
                            <ListGroup.Item>Data dołączenia: <Moment format="YYYY/MM/DD HH:mm">{this.state.user.date_joined}</Moment></ListGroup.Item>
                        </ListGroup>
                    </div>
                </div>
            )
        }
        
    }
}

export default withRouter(UserInfo);